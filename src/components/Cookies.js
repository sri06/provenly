import React, { Component } from "react";

class Cookies extends Component {
    render() {
        return (
            <div className="divContent">
                <p className="textContent1">Cookies</p>
                <p className="textContent2">We are committed to protecting the privacy of visitors to our websites.</p>
                <p className="textContent2">A cookie is a small file that can be placed on your computer’s hard disk or on a website server. Such cookies do not retrieve information about you stored on your hard drive and do not corrupt or damage your computer or computer files. You are not obliged to accept a cookie that we send to you, and you can modify your browser so that it will not accept cookies. If you do decide to disable cookies, you may not be able to access all areas of our website.</p>
            </div>
        )
    }
}

export default Cookies;