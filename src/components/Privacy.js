import React, { Component } from "react";

class Privacy extends Component {
    render() {
        return (
            <div className="divContent">
                <p className="textContent1">Privacy Policy</p>
                <p className="textContent2">We are committed to protecting your privacy.</p>
                <p className="textContent2">This policy may change from time to time, so please check it regularly.</p>
                <p className="textContent2">You are not required to provide any personal information on the public areas of this website. However, you may choose to do so by completing any application and/or contact form we place in parts of our website.</p>
                <p className="textContent2">We will only use your information to process the form. Please see the privacy wording on the form for a more detailed explanation of how your information will be used.</p>
                <p className="textContent2">Please note that for us to process any form we may share your information with our offices and branches and associated firms around the world and with third parties.</p>
                <p className="textContent2">We have taken steps to ensure that these entities protect your data. In relation to third parties, we will only disclose your information where you have given your consent or where we are required to do so by law, or where it is necessary for the purpose of, or in connection with, legal proceedings, or to exercise or defend our legal rights.</p>
            </div>
        )
    }
}

export default Privacy;