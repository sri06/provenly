import React, { Component } from "react";

class IPAddresses extends Component {
    render() {
        return (
            <div className="divContent">
                <p className="textContent1">IP Addresses</p>
                <p className="textContent2">When you visit our website, our server will record your IP address together with the date, time, and duration of your visit. An IP address is an assigned number that allows your computer to communicate over the Internet. It enables us to identify who visits our website. We use this information to compile statistical data on the use of our website and to track how users navigate through our site to enable us to evaluate and improve it.</p>
            </div>
        )
    }
}

export default IPAddresses;