import React, { useState, useRef } from 'react';
import "./Accordion.css";
import Collapse from 'react-bootstrap/Collapse';

const scrollToRef = (ref) => window.scrollTo(0, ref.current.offsetTop)

const Accordion = ({ title, title1, content }) => {
    const [isActive, setIsActive] = useState(false);
    const [open, setOpen] = useState(false);
    const cont = useRef(null);
    
    function executeScroll(){
        scrollToRef(cont);
    }

    return (
        <div className="accordion-item">
            <hr className="solid"></hr>
            <div  className="accordion-title" onClick={() => {executeScroll(); setIsActive(!isActive);}}>
                {isActive ? <div ref={cont} onClick={() => setOpen(!open)} aria-controls="example-fade-text" aria-expanded={open} > {title1} </div> : <div ref={cont} onClick={() => setOpen(!open)} aria-controls="example-fade-text" aria-expanded={open}> {title} </div>}
            </div>
            <Collapse in={open}>
                <div>
                    <div id="line">{open ? <hr className="solid"></hr> : ''}</div>
                    <div className="accordion-content collapsible" >{content}</div>
                </div>
            </Collapse>
        </div>
    );
};

export default Accordion;