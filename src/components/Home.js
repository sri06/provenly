import React from 'react';
import Accordion from './Accordion';
import { accordionData } from '../utils/content';

const Home = () => {
    return (
        <div>
            <div className="accordion">
                {accordionData.map(({ title, title1, content }) => (
                    <Accordion title={title} title1={title1} content={content}/>
                ))}
            </div>
            <hr className="solid"></hr>
        </div>
    );
};

export default Home;