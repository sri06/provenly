import React, { Component } from "react";

class RegulatedStatus extends Component {
    render() {
        return (
            <div className="divContent">
                <p className="textContent1">Regulated Status</p>
                <p className="textContent2">In the United Kingdom, The Provenly Company Limited is supervised by HMRC, our registration number is XLML00000150581 and confirmation of our registration and regulated status can be found at https://www.gov.uk/guidance/money-laundering-regulations-supervised- business-register</p>
                <p className="textContent2">In the United Arab Emirates, Provenly FZ-LLC is a Corporate Services Provider licensed by the Ras Al Khaimah Economic Zone, the Government of Ras Al Khaimah.</p>
            </div>
        )
    }
}

export default RegulatedStatus;