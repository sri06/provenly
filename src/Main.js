import React, { Component } from "react";
import './Main.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Col, Container, Row } from "react-bootstrap";
import { HashLink } from 'react-router-hash-link';
import {
    Route,
    HashRouter
} from "react-router-dom";
import Home from "./components/Home";
import Privacy from "./components/Privacy";
import Cookies from "./components/Cookies";
import IPAddresses from "./components/IPAddresses";
import RegulatedStatus from "./components/RegulatedStatus";

class Main extends Component {
    render() {
        return (
            <HashRouter>
                <Container fluid className="container1">
                    <Row>
                        <Col>
                        <a href="mailto:help@provenly.co" style={{ textDecoration: "none" }}> <h1 className="textHeader1">help@provenly.co</h1> </a>
                        </Col>
                    </Row>
                    <Row className="row1">
                        <Col className="header">
                            <h1 className="textHeader">Proven<span className="stroke">ly</span> IS A SINGLE SOLUTION FOR COMPANY ADMINISTRATION SERVICES </h1>
                        </Col>
                    </Row>
                    <Row id="section" className="row2">
                        <Col className="header1">
                            <Route exact path="/" component={Home} />
                            <Route path="/privacy" component={Privacy} />
                            <Route path="/ipaddresses" component={IPAddresses} />
                            <Route path="/cookies" component={Cookies} />
                            <Route path="/regulatedStatus" component={RegulatedStatus} />
                        </Col>
                    </Row>
                    <Row >
                        <Col sm className="container2">
                            <h1 className="textFooter1">Contact Us</h1>
                        </Col>
                    </Row>
                    <Row >
                        <Col sm className="container2">
                            <a href="mailto:help@provenly.co" style={{ color: "#000000" }}><h1 className="textFooter2">help@provenly.co</h1></a>
                            <a href="callto://+971506603496" style={{ color: "#000000" }}><h1 className="textFooter2">+971 50 660 3496</h1></a>
                            <svg id="linkedinweb" xmlns="http://www.w3.org/2000/svg" width="32" height="32" fill="currentColor" className="bi bi-linkedin textFooter2" viewBox="0 0 16 16">
                                <path d="M0 1.146C0 .513.526 0 1.175 0h13.65C15.474 0 16 .513 16 1.146v13.708c0 .633-.526 1.146-1.175 1.146H1.175C.526 16 0 15.487 0 14.854V1.146zm4.943 12.248V6.169H2.542v7.225h2.401zm-1.2-8.212c.837 0 1.358-.554 1.358-1.248-.015-.709-.52-1.248-1.342-1.248-.822 0-1.359.54-1.359 1.248 0 .694.521 1.248 1.327 1.248h.016zm4.908 8.212V9.359c0-.216.016-.432.08-.586.173-.431.568-.878 1.232-.878.869 0 1.216.662 1.216 1.634v3.865h2.401V9.25c0-2.22-1.184-3.252-2.764-3.252-1.274 0-1.845.7-2.165 1.193v.025h-.016a5.54 5.54 0 0 1 .016-.025V6.169h-2.4c.03.678 0 7.225 0 7.225h2.4z" />
                            </svg>
                        </Col>
                        <Col sm className="container2">
                            <h1 className="textFooter3">United Arab Emirates</h1>
                            <h1 className="textFooter3">The Offices 4,</h1>
                            <h1 className="textFooter3">One Central,</h1>
                            <h1 className="textFooter3">Dubai World Trade Centre,</h1>
                            <h1 className="textFooter3">Dubai</h1>
                            <br></br>
                            <h1 className="textFooter3">Hub 71,</h1>
                            <h1 className="textFooter3">Al Khatem Tower,</h1>
                            <h1 className="textFooter3">Al Falah St,</h1>
                            <h1 className="textFooter3">Jazeerat Al Maryah,</h1>
                            <h1 className="textFooter3">Abu Dhabi Global Market Square, </h1>
                            <h1 className="textFooter3">Abu Dhabi </h1>
                            <a href="mailto:uae@provenly.co" style={{ color: "#000000" }}><h1 className="textFooter3"> uae@provenly.co </h1></a>
                            <br></br>
                        </Col>
                        <Col sm className="container2">
                            <h1 className="textFooter3">United Kingdom</h1>
                            <h1 className="textFooter3">71-75 Shelton Street, </h1>
                            <h1 className="textFooter3">London, </h1>
                            <h1 className="textFooter3">WC2H 9JQ,</h1>
                            <h1 className="textFooter3">United Kingdom</h1>
                            <a href="mailto:uk@provenly.co" style={{ color: "#000000" }}><h1 className="textFooter3">uk@provenly.co</h1></a>
                            <br></br>
                            <h1 className="textFooter3">651 N Broad Street, </h1>
                            <h1 className="textFooter3">Suite 206, Middletown,</h1>
                            <h1 className="textFooter3">DE, 19709,</h1>
                            <h1 className="textFooter3">USA</h1>
                            <a href="mailto:usa@provenly.co" style={{ color: "#000000" }}><h1 className="textFooter3">usa@provenly.co</h1></a>
                            <br></br>
                        </Col>
                    </Row>
                    <Row id="linkedinmob">
                        <Col sm className="container2">
                            <svg xmlns="http://www.w3.org/2000/svg" width="64" height="64" fill="currentColor" className="bi bi-linkedin textFooter2" viewBox="0 0 16 16">
                                <path d="M0 1.146C0 .513.526 0 1.175 0h13.65C15.474 0 16 .513 16 1.146v13.708c0 .633-.526 1.146-1.175 1.146H1.175C.526 16 0 15.487 0 14.854V1.146zm4.943 12.248V6.169H2.542v7.225h2.401zm-1.2-8.212c.837 0 1.358-.554 1.358-1.248-.015-.709-.52-1.248-1.342-1.248-.822 0-1.359.54-1.359 1.248 0 .694.521 1.248 1.327 1.248h.016zm4.908 8.212V9.359c0-.216.016-.432.08-.586.173-.431.568-.878 1.232-.878.869 0 1.216.662 1.216 1.634v3.865h2.401V9.25c0-2.22-1.184-3.252-2.764-3.252-1.274 0-1.845.7-2.165 1.193v.025h-.016a5.54 5.54 0 0 1 .016-.025V6.169h-2.4c.03.678 0 7.225 0 7.225h2.4z" />
                            </svg>
                        </Col>
                    </Row>
                    <Row id="footerspace1">
                        <Col lg className="container2">
                            <HashLink style={{ textDecoration: "none" }} smooth to="/privacy#section"><h1 className="textFooter3">Privacy</h1></HashLink>
                        </Col>
                        <Col lg className="container2">
                            <HashLink style={{ textDecoration: "none" }} smooth to="/ipaddresses#section"><h1 className="textFooter3">IP Addresses</h1></HashLink>
                        </Col>
                        <Col lg className="container2">
                            <HashLink style={{ textDecoration: "none" }} smooth to="/cookies#section"><h1 className="textFooter3">Cookies</h1></HashLink>
                        </Col>
                        <Col lg className="container2">
                            <HashLink style={{ textDecoration: "none" }} smooth to="/regulatedStatus#section"><h1 className="textFooter3">Regulated Status</h1></HashLink>
                        </Col>
                        <Col lg className="container2">
                            <HashLink style={{ textDecoration: "none" }} smooth to="/#section"><h1 className="textFooter3">Provenly 2021©</h1></HashLink>
                        </Col>
                    </Row>
                </Container>
            </HashRouter>
        );
    }
}

export default Main;