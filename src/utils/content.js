
export const accordionData = [
  {
    title: <h1 className="textHeader2">PROVEN <span className="stroke" id="prov">ADMINISTRATION</span></h1>,
    title1: <h1 className="textHeader2">PROVEN ADMINISTRATION</h1>,
    content: <div className="divContent">
      <p className="textContent1">Company administrative solutions across all entity types and jurisdictions.</p>
      <ul>
        <li className="textContent2">Corporate Secretarial Services: Ensuring compliance with local regulations, across all entities</li>
        <li className="textContent2">Fiduciary Services: Implementing structures, ultimate beneficial owner registration, registered address, process agent, and board support services</li>
        <li className="textContent2">International Regulatory Compliance: Common Reporting Standards (CRS), Foreign Account Tax Compliance Act (FATCA), Country-by-Country notification and OECD Local and Master File, Know Your Customer (KYC) obligations and Legal Entity Identifier</li>
      </ul>
    </div>
  },
  {
    title: <h1 className="textHeader2">PROVEN <span className="stroke">ACCONTING</span></h1>,
    title1: <h1 className="textHeader2">PROVEN ACCONTING</h1>,
    content: <div className="divContent">
      <p className="textContent1">We organise and manage accounting and tax reporting activities, locally and worldwide.</p>
      <ul>
        <li className="textContent2">Bookkeeping and reporting: Monthly, quarterly, and annual financial statements that comply with recognised accounting standards (local GAAP, US GAAP and IFRS)</li>
        <li className="textContent2">Annual and statutory return filings</li>
        <li className="textContent2">Value Added Taxes, registration, and administration</li>
        <li className="textContent2">Tax advisory</li>
        <li className="textContent2">Tax compliance</li>
        <li className="textContent2">Audit and internal control</li>
      </ul></div>
  },
  {
    title: <h1 className="textHeader2">PROVEN <span className="stroke">PAYROLL</span></h1>,
    title1: <h1 className="textHeader2">PROVEN PAYROLL</h1>,
    content: <div className="divContent">
      <p className="textContent1">Payroll processing locally and worldwide.</p>
      <ul>
        <li className="textContent2">Fully managed local and expat payroll management</li>
        <li className="textContent2">Group pension schemes</li>
        <li className="textContent2">Group business and health insurances</li>
        <li className="textContent2">Administrative services</li>
        <li className="textContent2">Contracts, handbooks and policies, expense, and benefit administration, leave management, and visa applications</li>
      </ul></div>
  },
  {
    title: <h1 className="textHeader2">PROVEN <span className="stroke">BANKING</span></h1>,
    title1: <h1 className="textHeader2">PROVEN BANKING</h1>,
    content: <div className="divContent">
      <p className="textContent1">We open multicurrency business and private bank accounts from within 72 hours, with over 100+ financial institution partnerships.</p>
      <p className="textContent1"> Our banking solution ensures a financial institution is matched to your exact private or commercial requirement.</p>
      <p className="textContent1"> Our financial institution partnerships include transaction specific corporate bank accounts to highly personalised and discreet private bank investment accounts.</p>
    </div>
  },
  {
    title: <h1 className="textHeader2">PROVEN <span className="stroke">REGULATION</span></h1>,
    title1: <h1 className="textHeader2">PROVEN REGULATION</h1>,
    content: <div className="divContent">
      <p className="textContent1">A “Start-up” Service: We apply for licenses to regulators and supply a full suite of ongoing administrative, tax, accounting, banking, HR, and compliance support.</p>
    </div>
  }
];